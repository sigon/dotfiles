# SPARK


Edit config file:

```
$ cd /usr/local/spark/conf

$ sudo cp spark-env.sh.template spark-env.sh

$ sudo gedit /usr/local/spark/conf/spark-env.sh
```

Add these lines:

export SPARK_MASTER_IP=127.0.0.1
export SPARK_WORKER_CORES=1
export SPARK_WORKER_MEMORY=500mb
export SPARK_WORKER_INSTANCES=2

## Launching Spark Shell

Now when we are right outside the spark directory, run the following command to open apark shell:

```
$ /usr/local/spark/bin/spark-shell
```

## Run Python Spark Shell


```
$ /usr/local/spark/bin/pyspark

```
You'll see something like this:

```
Welcome to
      ____              __
     / __/__  ___ _____/ /__
    _\ \/ _ \/ _ `/ __/  '_/
   /__ / .__/\_,_/_/ /_/\_\   version 2.3.2
      /_/

Using Python version 3.6.3 (default, Oct 13 2017 12:02:49)
SparkSession available as 'spark'.
>>>
```
type Ctrl + d to quit

## run spark shells



```
$ ~ /usr/local/spark/bin/spark-shell                                       19:19:58
2018-10-29 19:22:08 WARN  Utils:66 - Your hostname, sigon resolves to a loopback address: 127.0.1.1; using 1?.???.?.?? instead (on interface wlp4s0)
2018-10-29 19:22:08 WARN  Utils:66 - Set SPARK_LOCAL_IP if you need to bind to another address
2018-10-29 19:22:08 WARN  NativeCodeLoader:62 - Unable to load native-hadoop library for your platform... using builtin-java classes where applicable
Setting default log level to "WARN".
To adjust logging level use sc.setLogLevel(newLevel). For SparkR, use setLogLevel(newLevel).
Spark context Web UI available at http://1?.???.?.??:4040
Spark context available as 'sc' (master = local[*], app id = local-1540837333311).
Spark session available as 'spark'.
Welcome to
      ____              __
     / __/__  ___ _____/ /__
    _\ \/ _ \/ _ `/ __/  '_/
   /___/ .__/\_,_/_/ /_/\_\   version 2.3.2
      /_/

Using Scala version 2.11.8 (OpenJDK 64-Bit Server VM, Java 1.8.0_181)
Type in expressions to have them evaluated.
Type :help for more information.

scala>

```

type :q to quit

You can see the Spark shell context web UI  in the browser on your IP address on port 4040


## Start a standalone master server.


```
$ /usr/local/spark/sbin/start-master.sh  
```
At this point you can browse to http://127.0.0.1:8080/ to view the status screen.


And stop it:

```
$ /usr/local/spark/sbin/stop-master.sh  
```
