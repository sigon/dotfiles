# FISH SHELL

https://fishshell.com/

Exports (Shell Variables)

Unlike other shells, fish does not have an export command. Instead, a variable is exported via an option to set, either --export or just -x.

> set -x MyVariable SomeValue
> env | grep MyVariable
MyVariablem=SomeValue


Some variables, like $PATH, have multiple values. During variable expansion, the variable expands to become multiple arguments:

> echo $PATH
/usr/bin /bin /usr/sbin /sbin /usr/local/bin

You can append (or prepend) to a list by setting the list to itself, with some additional arguments. Here we append /usr/local/bin to $PATH:

> set PATH $PATH /usr/local/bin


### $PATH

$PATH is an environment variable containing the directories in which fish searches for commands. Unlike other shells, $PATH is a list, not a colon-delimited string.

To prepend /usr/local/bin and /usr/sbin to $PATH, you can write:

> set PATH /usr/local/bin /usr/sbin $PATH

## Startup (Where's .bashrc?)

fish starts by executing commands in ~/.config/fish/config.fish. You can create it if it does not exist.

You can add variables directly in config.fish, like you might do in other shells with .profile.

## The missing config to reuse Bash variables and aliases in Fish

https://hospodarets.com/fish-shell-the-missing-config

To reuse Bash vars and aliases we would just parse ~/.bash_profile and reapply them for Fish. For that add the following scripts to the Fish config at ~/.config/fish/config.fish
