# Unix shell initialization

(NOTE: taken from

  https://github.com/sstephenson/rbenv/wiki/Unix-shell-initialization

  https://unix.stackexchange.com/questions/50665/what-is-the-difference-between-interactive-shells-login-shells-non-login-shell#50667

)

Shell initialization files are ways to persist common shell configuration, such as:


* `$PATH` and other environment variables
* shell prompt
* shell tab-completion
* aliases, functions
* key bindings


## Shell modes

Which initialization files get sourced by the shell is dependent on the
combination of modes in which a particular shell process runs.

A shell is the generic name for any program that gives you a text-interface to interact with the computer. You type a command and the output is shown on screen.

Many shells have scripting abilities: Put multiple commands in a script and the shell executes them as if they were typed from the keyboard. Most shells offer additional programming constructs that extend the scripting feature into a programming language.

On most Unix/Linux systems multiple shells are available: bash, csh, ksh, sh, tcsh, zsh just to name a few. They differ in the various options they give the user to manipulate the commands and in the complexity and capabilities of the scripting language.

* **interactive**: As the term implies: Interactive means that the commands are run with user-interaction from keyboard. E.g. the shell can prompt the user to enter input.

* **Non-interactive** : the shell is probably run from an automated process so it can't assume if can request input or that someone will see the output. E.g Maybe it is best to write output to a log-file.

* **login**: Means that the shell is run as part of the login of the user to the system. Typically used to do any configuration that a user needs/wants to establish his work-environment.

* **Non-login**: Any other shell run by the user after logging on, or which is run by any automated process which is not coupled to a logged in user.

Here are some common operations and shell modes they result in:

* log in to a remote system via SSH:
  **login + interactive**
* execute a script remotely, e.g. `ssh user@host 'echo $PWD'` or with
  [Capistrano][]: **non‑login,&nbsp;non‑interactive**
* execute a script remotely and request a terminal, e.g. `ssh user@host -t 'echo $PWD'`: **non-login,&nbsp;interactive**
* start a new shell process, (open a new terminal), e.g.  `bash`:
  **non‑login, interactive**
* run a script, `bash myscript.sh`:
  **non‑login, non‑interactive**
* run an executable with `#!/usr/bin/env bash` shebang:
  **non‑login, non‑interactive**
* open a new graphical terminal window/tab:
  * on Mac OS X: **login, interactive**
  * on Linux: **non‑login, interactive**


## Shell init files

In order of activation:

### bash

1. **login** mode:
   1. `/etc/profile`
   2. `~/.bash_profile`, `~/.bash_login`, `~/.profile` (only first one that exists)
2. interactive **non-login**:
   1. `/etc/bash.bashrc` (some Linux; not on Mac OS X)
   2. `~/.bashrc`
3. **non-interactive**:
   1. source file in `$BASH_ENV`

### [fish][]

1. `<install-prefix>/config.fish`
2. `/etc/fish/config.fish`
3. `~/.config/fish/config.fish`

## Environment variables

Environment variables consist of names that have values assigned to them

The printenv command prints the names and values of all currently defined environment variables:

```
$ printenv
```

To examine the value of a particular variable, we can specify its name to the printenv command:

```
$ printenv PATH
```

Another way to achieve that is to use the dollar sign ($), as used in the following example:

```
$ echo $PATH
```

## Configuration variables

Files in /etc are for all users
Files in ~/ are for one user

PATH


/usr/sbin:/usr/bin:/sbin:/bin


When you type a command to run, the system looks for it in the directories specified by PATH in the order specified
